// 1. Explain exception handling.

const { add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");


function main() {
    try {
        // foo calls bar, which raises an exception.
        // foo does not handle the exception with a try-catch,
        // so the exception continues up the call stack until it
        // gets here, where it is caught in the catch statement below.
        foo();
        console.log(
            "This is never printed because the exception raised in foo will skip the rest of the try block."
        );
    } catch (e) {
        console.log(e);
    }
}

function foo() {
    bar();
}

function bar() {
    throw {
        "error": "example",
        "message": "This demonstrates bar raising and exception"
    };
    console.log(
        "This never prints because the raised exception will skip the rest of the function."
    );
}



main();

//Answer: Exception Handling allows the code to throw the function bar() since an error occured, which prints out the message of bar(). Main() could never work since the function in foo() will always run bar() and so that will never fully utilize the try block.


