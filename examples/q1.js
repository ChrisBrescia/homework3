const {add1,
	async_add1,
	raise_exception,
	async_raise_exception,
	async_reject} = require("./lib.js")

async function main() {
	const answer = await async_add1(12);
	const answer2 = await async_add1(12);
}

main()

//Answer: The use of the await method is to have the program await until the promise object has been settled properly. This is because the program runs through the code and if the promise is unsettled, the await method will wait an approximate set time until the add1 function. 

