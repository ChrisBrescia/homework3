const { add1,
        async_add1,
        raise_exception,
        async_raise_exception,
        async_reject } = require("./lib.js");

async function main() {
	const answer = async_add1(12);
	const answer2 = async_add1(12);
	console.log("18 + 9 = ", answer);
	console.log(" 9 + 10 = ", answer2);
}


main();

// In this example, we add the "answer" and "answer2" to the code which is not a problem, the returned code will return a promise, more importantly it will show the promise status, which in our case will be "pending."
