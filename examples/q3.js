const { add1,
        async_add1,
        raise_exception,
        async_raise_exception,
        async_reject } = require("./lib.js");

function main() {
	const answer = await add1(12);
	console.log(answer);

}

main()

// Doing this program will return with an error, more specifically a syntax error. This is because of the await function, since it is only able to funciton properly with an async function.
