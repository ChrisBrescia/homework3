const { add1,
	async_add1,
	raise_exception,
	async_raise_exception,
	async_reject } = require("./lib.js");

function main() {
	async_reject("This will first return a promise, then a reject")
		.then(() => console.log("This will not be called"))
		.catch((x) => console.log("Here the catch will be called"))
		.then(() => console.log("Here we can see the reject was handeled so this will run"))
		.catch((x) => console.log("This method will not run since there is no other rejects to be handled"))
}

main();

//answer: This function utilizes the .then after a .catch is called. For this example, the first catch will run properly and then the .then() will run after this. It is important to note that the second .catch will not run becuase the first .catch handeled the only reject present.
