// 3. What happens when a promise object that returns is rejected?

const { add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");


function main() {
    async_reject("This method returns a promise, and will reject")
        .then(() => console.log("this will not be called"))
        .then(() => console.log("this will not be called either"))
        .catch((e) => console.log("SHOULD SEE: This catch will be called"))
        .then(() => console.log("SHOULD SEE: This then WILL run because the reject has been handled"))
        .catch((e) => console.log("This catch will NOT be called because the reject was handled above and no further rejects occured."))
}

main();

//Answer: Based on the code above when a promise object that is rejected is returned it will iterate through the main async reject methods which in this case means that it will reach the first .catch and console log the argument. Since the first catch is utilized then the .then afterwards of the first catch will run properly.  This code is important since it shows the use of the .catch methods. Here the second .catch method will never be used since the first .catch will catch any rejections.
